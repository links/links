//
//  LKProfileViewController.h
//  Links
//
//  Created by rinatear on 13. 2. 11..
//  Copyright (c) 2013년 links. All rights reserved.
//

#import <UIKit/UIKit.h>
@class LKProfileView;
@interface LKProfileViewController : UIViewController
{
    BOOL mCheckBoxSelected;
    UIButton *mCheckbox;
    LKProfileView *mProfileView;
}

@end
