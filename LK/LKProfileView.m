//
//  LKProfileView.m
//  Links
//
//  Created by rinatear on 13. 2. 25..
//  Copyright (c) 2013년 links. All rights reserved.
//

#import "LKProfileView.h"
#import "LKProfileImageView.h"
#import "LKProfileDetailView.h"
@implementation LKProfileView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self attachProfileInfoView];
    }
    return self;
}
/*
 UIImage *sBgImage = [UIImage imageNamed:@"icon.png"];
 UIImageView *sBgImageView = [[[UIImageView alloc] initWithImage: sBgImage] autorelease];
 [sBgImageView sizeToFit];
 */


- (void)attachProfileInfoView
{
    UIImage *sBgImage1 = [UIImage imageNamed:@"icon.png"];
    mProfileImage1 = [[[UIImageView alloc] initWithImage: sBgImage1] autorelease];
    [self addSubview:mProfileImage1];
//    [sBgImageView sizeToFit];
    [mProfileImage1 setFrame:CGRectMake(10, 50, 50, 50)];
    
    UIImage *sBgImage2 = [UIImage imageNamed:@"icon.png"];
    mProfileImage2 = [[[UIImageView alloc] initWithImage: sBgImage2] autorelease];
    [self addSubview:mProfileImage2];
    //    [sBgImageView sizeToFit];
    [mProfileImage2 setFrame:CGRectMake(80, 50, 50, 50)];
    
    UIImage *sBgImage3 = [UIImage imageNamed:@"icon.png"];
    mProfileImage3 = [[[UIImageView alloc] initWithImage: sBgImage3] autorelease];
    [self addSubview:mProfileImage3];
    //    [sBgImageView sizeToFit];
    [mProfileImage3 setFrame:CGRectMake(170, 50, 50, 50)];
    
    LKProfileDetailView *sProfileDetailView = [[[LKProfileDetailView alloc] initWithFrame:CGRectMake(20, 70, 250, 220)] autorelease];
    [self addSubview:sProfileDetailView];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
