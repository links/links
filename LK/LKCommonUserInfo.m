//
//  LKCommonConfig.m
//  Links
//
//  Created by rinatear on 13. 3. 12..
//  Copyright (c) 2013년 links. All rights reserved.
//

#import "LKCommonUserInfo.h"

@implementation LKCommonUserInfo

-(void)login:(NSDictionary*)aJsonResult
{
    NSString *aAuthKey = [aJsonResult valueForKey:kAuthKey];
    NSString *aDbIndex = [aJsonResult valueForKey:kDbIndex];
    NSString *aLastLogin = [aJsonResult valueForKey:kLastLogin];
    NSString *aUserId = [aJsonResult valueForKey:kUserId];
    NSString *aUserIndex = [aJsonResult valueForKey:kUserIndex];
    NSString *aUserProfileId = [aJsonResult valueForKey:kUserProfileId];
    
    NSUserDefaults *UD = [NSUserDefaults standardUserDefaults];
    if(UD) {
        [UD setObject:[NSString stringWithFormat:@"%@", aAuthKey] forKey:kAuthKey];
        [UD setObject:[NSString stringWithFormat:@"%@", aDbIndex] forKey:kDbIndex];
        [UD setObject:[NSString stringWithFormat:@"%@", aLastLogin] forKey:kLastLogin];
        [UD setObject:[NSString stringWithFormat:@"%@", aUserId] forKey:kUserId];
        [UD setObject:[NSString stringWithFormat:@"%@", aUserIndex] forKey:kUserIndex];
        [UD setObject:[NSString stringWithFormat:@"%@", aUserProfileId] forKey:kUserProfileId];
        
        [UD synchronize];
    }
}

-(void)logout
{
    NSUserDefaults *UD = [NSUserDefaults standardUserDefaults];
    if(UD) {
        [UD removeObjectForKey:kAuthKey];
        [UD removeObjectForKey:kDbIndex];
        [UD removeObjectForKey:kLastLogin];
        [UD removeObjectForKey:kUserId];
        [UD removeObjectForKey:kUserIndex];
        [UD removeObjectForKey:kUserProfileId];
        
        [UD synchronize];
    }
}

-(void)removeUserDefaultByKey:(NSString*)aKey
{
    
}

-(BOOL)checkLoggedIn{
    NSUserDefaults *UD = [NSUserDefaults standardUserDefaults];
    if(UD) {
        if([UD objectForKey:kAuthKey] && [UD objectForKey:kDbIndex] && [UD objectForKey:kUserId])
        {
            return YES;
        }
    }
    return NO;
}

-(NSString*)objectForKey:(NSString*)aKey
{
    NSUserDefaults *UD = [NSUserDefaults standardUserDefaults];
    if(UD) {
        return [UD objectForKey:aKey];
    }
    return @"";
}
@end
