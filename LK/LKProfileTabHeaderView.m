//
//  LKProfileTabHeaderView.m
//  Links
//
//  Created by rinatear on 13. 3. 13..
//  Copyright (c) 2013년 links. All rights reserved.
//

#import "LKProfileTabHeaderView.h"

@implementation LKProfileTabHeaderView

@synthesize delegate = mDelegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self attachHeaderView];
    }
    return self;
}


- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        [self attachHeaderView];
    }
    return self;
}


- (void)attachHeaderView
{
    [self setFrame:CGRectMake(0, 0, 320, 40)];
    UIImage *sBackImage = [UIImage imageNamed:@"bg_interiorshop.jpg"];
    //    UIImage *scaledImag = [UIImage imageWithCGImage:[sBackImage CGImage] scale:(sBackImage.scale) orientation:(sBackImage.imageOrientation)];
    UIImageView *sBaseView = [[UIImageView alloc] initWithImage:sBackImage];
    [self addSubview:sBaseView];
    float sWidth = self.frame.size.width;
    float sHeight = self.frame.size.height;
    
    //title bar with button
    UILabel *sTitle = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, sWidth, 40)] autorelease];
    [sTitle setText:@"profile"];
    [sTitle setTextAlignment:NSTextAlignmentCenter];
    [sTitle setBackgroundColor:[UIColor grayColor]];
    
    UIButton *sEditBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [sTitle addSubview:sEditBtn];
    [sEditBtn setTitle:@"Edit" forState:UIControlStateNormal];
    
    UIImage *sEditBtnImage = [UIImage imageNamed:@"btn_vote_on.png"];
    [sEditBtn setBackgroundImage:sEditBtnImage forState:UIControlStateNormal];
    [sEditBtn sizeToFit];
    [sEditBtn setFrame:CGRectMake(sWidth-sEditBtn.frame.size.width, 0, sEditBtn.frame.size.width, sEditBtn.frame.size.height)];
    [sEditBtn addTarget:self action:@selector(editButtonDidTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sTitle];
    [sTitle setUserInteractionEnabled:YES];
}

- (void)editButtonDidTapped:(id)aSender
{
    if([mDelegate respondsToSelector:@selector(editButtonDidTapped:)])
    {
        [mDelegate performSelector:@selector(editButtonDidTapped:)];
    }
}

@end
