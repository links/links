//
//  LKProfileTabHeaderView.h
//  Links
//
//  Created by rinatear on 13. 3. 13..
//  Copyright (c) 2013년 links. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LKProfileTabHeaderView : UIView
{
    id mDelegate;
}
@property (nonatomic, retain) id delegate;
@end
