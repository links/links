//
//  LKAgreementsViewController.m
//  Links
//
//  Created by rinatear on 13. 2. 7..
//  Copyright (c) 2013년 links. All rights reserved.
//

#import "LKAgreementsViewController.h"
#import "LKJoinViewController.h"

@interface LKAgreementsViewController ()

@end

@implementation LKAgreementsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title=@"links";

    float sFrameWidth = [self view].frame.size.width;
    float sFrameHeight = [self view].frame.size.height;
    
    UIImageView *backImage = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, sFrameWidth, sFrameHeight)] autorelease];
    [backImage setBackgroundColor:[UIColor redColor]];
    [[self view] addSubview:backImage];

    UILabel *sTitle = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, sFrameWidth, 30)] autorelease];
    [sTitle setText:@"User Agreements"];
    [sTitle setTextAlignment:NSTextAlignmentCenter];
    
    UILabel *sServiceAgreementsTitle = [[[UILabel alloc] init] autorelease];
    [sServiceAgreementsTitle setFrame:CGRectMake(0, 40, sFrameWidth, 30)];
    [sServiceAgreementsTitle setText:@"serviceAgreements"];

    UITextView *sServiceAgreementsDesc = [[[UITextView alloc] init] autorelease];
    [sServiceAgreementsDesc setFrame:CGRectMake(0, 70, sFrameWidth, 100)];
    [sServiceAgreementsDesc setText:@"sServiceAgreementsDesc adfljklj \nasfd\nsaaaa"];
    [sServiceAgreementsDesc setBackgroundColor:[UIColor grayColor]];
    [sServiceAgreementsDesc setEditable:NO];
    
    UILabel *sPrivateTitle = [[[UILabel alloc] init] autorelease];
    [sPrivateTitle setFrame:CGRectMake(0, 180, sFrameWidth, 30)];
    [sPrivateTitle setText:@"Private Title"];
    UITextView *sPrivateDesc = [[[UITextView alloc] init] autorelease];
    [sPrivateDesc setFrame:CGRectMake(0, 210, sFrameWidth, 100)];
    [sPrivateDesc setText:@"sPrivateDesc adfljklj"];
    [sPrivateDesc setBackgroundColor:[UIColor grayColor]];
    [sPrivateDesc setEditable:NO];
    

    
    UIButton *sJoinButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [sJoinButton setTitle:@"agree&join" forState:UIControlStateNormal];
    [sJoinButton setFrame:CGRectMake(sFrameWidth*0.1, sFrameHeight*0.8, sFrameWidth*0.8, 30)];
    [sJoinButton addTarget:self action:@selector(agreeBtn:) forControlEvents:UIControlEventTouchUpInside];
    
    [[self view] addSubview:sTitle];
    [[self view] addSubview:sServiceAgreementsTitle];
    [[self view] addSubview:sServiceAgreementsDesc];
    [[self view] addSubview:sPrivateTitle];
    [[self view] addSubview:sPrivateDesc];
    [[self view] addSubview:sJoinButton];
    
    [[self navigationController]setNavigationBarHidden:NO];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) agreeBtn:(id)sender
{
    NSLog(@"agreeBtn");
    LKJoinViewController *sJoinViewController = [[[LKJoinViewController alloc] init] autorelease];
    [[self navigationController] pushViewController:sJoinViewController animated:YES];
}
@end
