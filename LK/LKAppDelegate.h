//
//  LKAppDelegate.h
//  LK
//
//  Created by rinatear on 13. 2. 5..
//  Copyright (c) 2013년 links. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LKViewController;

@interface LKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) LKViewController *viewController;

@end
