//
//  LKProfileImageView.m
//  Links
//
//  Created by rinatear on 13. 2. 25..
//  Copyright (c) 2013년 links. All rights reserved.
//

#import "LKProfileImageView.h"

@implementation LKProfileImageView

- (id)initWithFrame:(CGRect)aFrame
{
    self = [super initWithFrame:aFrame];
    if (self) {
        // Initialization code
        [self profileIamgeWithFrame:aFrame];
    }
    return self;
}

-(void)profileIamgeWithFrame:(CGRect)aFrame
{
    UIImage *sBgImage = [UIImage imageNamed:@"icon.png"];
    UIImageView *sBgImageView = [[[UIImageView alloc] initWithImage: sBgImage] autorelease];
    [sBgImageView sizeToFit];
    [self addSubview:sBgImageView];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
