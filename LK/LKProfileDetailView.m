//
//  LKProfileDetailView.m
//  Links
//
//  Created by rinatear on 13. 2. 25..
//  Copyright (c) 2013년 links. All rights reserved.
//

#import "LKProfileDetailView.h"

@implementation LKProfileDetailView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self attachDetailView:frame];
    }
    return self;
}

-(void) attachDetailView:(CGRect)aFrame
{
    UIView *sBGTest = [[UIView alloc] initWithFrame:aFrame];
    [sBGTest setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:sBGTest];
    
//    UIImageView *sBgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon.png"]];
//    [sBgView setFrame:CGRectMake(0, 0, 150, 150)];
//    [self addSubview:sBgView];
    
    UILabel *sNameLabel = [[[UILabel alloc] init] autorelease];
    [sNameLabel setText:@"Name"];
    [sNameLabel sizeToFit];
    [sBGTest addSubview:sNameLabel];
    
    UILabel *sUserName = [[[UILabel alloc] init] autorelease];
    [sUserName setText:@"rinatear"];
    [sUserName sizeToFit];
    [sBGTest addSubview:sUserName];
    [sUserName setFrame:CGRectMake(60, 0, sUserName.frame.size.width, sUserName.frame.size.height)];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
