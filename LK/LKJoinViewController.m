//
//  LKJoinViewController.m
//  Links
//
//  Created by rinatear on 13. 2. 11..
//  Copyright (c) 2013년 links. All rights reserved.
//

#import "LKJoinViewController.h"

@interface LKJoinViewController ()
{
    UITextField *emailText;
    UITextField *passwd1Text;
    UITextField *passwd2Text;
}
@end

@implementation LKJoinViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:self.view.window];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:self.view.window];
    
    float sWidth= [self view].bounds.size.width;
    float sHeight= [self view].bounds.size.height;
    [self view].backgroundColor=[UIColor blackColor];
    emailText = [[[UITextField alloc] initWithFrame:CGRectMake(sWidth*0.1, sHeight*0.5, sWidth*0.8, 30)] autorelease];
    emailText.placeholder=@"email";
    emailText.textColor = [UIColor lightGrayColor];
    emailText.delegate = self;
    [[self view] addSubview:emailText];
    emailText.returnKeyType = UIReturnKeyDone;
    emailText.keyboardType = UIKeyboardTypeEmailAddress;
    emailText.backgroundColor = [UIColor whiteColor];
    [[self view] addSubview:emailText];
    
    passwd1Text = [[[UITextField alloc] initWithFrame:CGRectMake(sWidth*0.1, sHeight*0.6, sWidth*0.8, 30)] autorelease];
    passwd1Text.textColor = [UIColor lightGrayColor];
    passwd1Text.placeholder=@"password1";
    passwd1Text.delegate = self;
    passwd1Text.returnKeyType = UIReturnKeyDone;
    passwd1Text.keyboardType = UIKeyboardTypeAlphabet;
    passwd1Text.secureTextEntry=YES;
    passwd1Text.backgroundColor = [UIColor whiteColor];
    [[self view] addSubview:passwd1Text];
    
    passwd2Text = [[[UITextField alloc] initWithFrame:CGRectMake(sWidth*0.1, sHeight*0.7, sWidth*0.8, 30)] autorelease];
    passwd2Text.textColor = [UIColor lightGrayColor];
    passwd2Text.placeholder=@"password2";
    passwd2Text.delegate = self;
    passwd2Text.returnKeyType = UIReturnKeyDone;
    passwd2Text.keyboardType = UIKeyboardTypeAlphabet;
    passwd2Text.secureTextEntry=YES;
    passwd2Text.backgroundColor = [UIColor whiteColor];
    [[self view] addSubview:passwd2Text];
    
    
    UIButton *sLoginBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [sLoginBtn setTitle:@"join" forState:UIControlStateNormal];
    [sLoginBtn addTarget:self action:@selector(loginBtn:) forControlEvents:UIControlEventTouchUpInside];
    [[self view] addSubview:sLoginBtn];
    [sLoginBtn setFrame:CGRectMake(sWidth*0.1, sHeight*0.8, sWidth*0.3, 30)];
    
    UIButton *sCancelBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [sCancelBtn setTitle:@"Cancel" forState:UIControlStateNormal];
    [sCancelBtn addTarget:self action:@selector(cancelBtn:) forControlEvents:UIControlEventTouchUpInside];
    [[self view] addSubview:sCancelBtn];
    [sCancelBtn setFrame:CGRectMake(sWidth*0.5, sHeight*0.8, sWidth*0.3, 30)];
    
}

- (void) keyboardWillShow:(NSNotification *) notification
{
    NSTimeInterval sAnimationDuration;
    CGRect sFrameBegin;
    CGRect sFrameEnd;
    NSDictionary *sUserInfo = [notification userInfo];
    [[sUserInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] getValue:&sFrameBegin];
    [[sUserInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&sFrameEnd];
    [[sUserInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&sAnimationDuration];
    
    if(self.view.frame.origin.y >= 0)
    {
        [self setViewMoviedUp:YES height:sFrameBegin.size.height duration:sAnimationDuration];
    }
    else
    {
        [self setViewMoviedUp:NO height:sFrameEnd.size.height duration:sAnimationDuration];
    }
}


- (void) keyboardWillHide:(NSNotification *) notification
{
    NSTimeInterval sAnimationDuration;
    CGRect sFrameBegin;
    CGRect sFrameEnd;
    NSDictionary *sUserInfo = [notification userInfo];
    [[sUserInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] getValue:&sFrameBegin];
    [[sUserInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&sFrameEnd];
    [[sUserInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&sAnimationDuration];
    
    if(self.view.frame.origin.y >= 0)
    {
        [self setViewMoviedUp:YES height:sFrameBegin.size.height duration:sAnimationDuration];
    }
    else
    {
        [self setViewMoviedUp:NO height:sFrameEnd.size.height duration:sAnimationDuration];
    }
}
- (void) setViewMoviedUp:(BOOL)movedUp height:(float)height duration:(NSTimeInterval)duration
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:duration];
    CGRect rect = self.view.frame;
    if(movedUp)
    {
        rect.origin.y -= height;
        rect.size.height += height;
    } else {
        rect.origin.y += height;
        rect.size.height -= height;
    }
    self.view.frame = rect;
    [UIView commitAnimations];
    
}
- (void) loginBtn: (id) sender
{
    //    [self view].resignFirstResponder;
    NSLog(@"loginBtn. email:%@, pw:%@,%@", emailText.text, passwd1Text.text, passwd2Text.text);
}

- (void) cancelBtn: (id) sender
{
    //    [self view].resignFirstResponder;
    NSLog(@"cancelBtn");
    [[self navigationController] popToRootViewControllerAnimated:YES];
}
- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if(textField == passwd1Text) {
        NSLog(@"pwText return");
        [self loginBtn:passwd1Text];
    }
    //button
}

- (void) viewWillDisappear:(BOOL)animated
{
    [self view].resignFirstResponder;
}

- (void) viewWillAppear:(BOOL)animated
{
    [[self navigationController] setNavigationBarHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
