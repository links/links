//
//  LKConfigViewController.m
//  Links
//
//  Created by rinatear on 13. 2. 14..
//  Copyright (c) 2013년 links. All rights reserved.
//

#import "LKConfigViewController.h"
#import "LKCommonUserInfo.h"

@interface LKConfigViewController ()

@end

@implementation LKConfigViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id) init
{
    self = [super init];
    if (self) {
        UIImage *sImage = [UIImage imageNamed:@"gacha_ball_small_green.png"];
         UIImage *scaledImage = [UIImage imageWithCGImage:[sImage CGImage] scale:(sImage.scale * 5.0) orientation:(sImage.imageOrientation)];
        //        sImage.size = CGSizeMake(10, 10);
        self.tabBarItem = [[[UITabBarItem alloc] initWithTitle:@"config" image:scaledImage tag:0] autorelease];
        self.tabBarItem.badgeValue=@"1";
//        self.navigationItem.prompt=@"aaa";
    }
    return self;
}


- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:NO];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIButton *sLogoutBtn = [[UIButton buttonWithType:UIButtonTypeRoundedRect] autorelease ];
    [[self view] addSubview:sLogoutBtn];
    [sLogoutBtn setTitle:@"logout" forState:UIControlStateNormal];
    [sLogoutBtn sizeToFit];
    [sLogoutBtn addTarget:self action:@selector(logoutBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)logoutBtnTapped:(id)aSender
{
    NSUserDefaults *UD = [NSUserDefaults standardUserDefaults];
    if(UD) {
//        [UD setObject:[NSString stringWithFormat:@"%@", gSession] forKey:kAuthKey];
        [UD removeObjectForKey:kAuthKey];
        [UD synchronize];
    }
    [[self navigationController] popToRootViewControllerAnimated:YES];
}

-(void) viewDidAppear:(BOOL)animated
{
    self.tabBarItem.badgeValue=nil;
}

- (void) homeBtnDidTapped:(id)selector
{
    NSLog(@"ddd");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
