//
//  LKHttpRequest.m
//  Links
//
//  Created by rinatear on 13. 2. 12..
//  Copyright (c) 2013년 links. All rights reserved.
//

#import "LKHttpRequest.h"
#import "LKCommonUserInfo.h"

static NSString* const gServerDomain = @"https://linkmore.nextio.co.kr/";

@implementation LKHttpRequest


//override "didReceiveFinished:"
//- (void) didReceiveFinished:(NSData *)aResult
//{
//    NSLog(@"Received data:\n%@", aResult);
//    NSError *sError;
//    NSDictionary *sJsonResult = [NSJSONSerialization JSONObjectWithData:aResult options:kNilOptions error:&sError];
//    NSLog(@"Received Json:\n%@", sJsonResult);
//    
//    if([[sJsonResult valueForKey:@"result"] isEqualToString:@"fail"])
//    {
//        NSLog(@"reason:%@", [sJsonResult valueForKey:@"reason"]);
//        NSLog(@"login fail");
//    }
//}

- (BOOL)requestWithURL:(NSString *)aURL
                 method:(NSString *)aMethod
                  param:(NSData *)aPostData
                headers:(NSDictionary *)aHeaders
{
    
    NSMutableString *sRequestURL = [[[NSMutableString alloc]initWithString:gServerDomain] autorelease];
    [sRequestURL appendString:aURL];
    NSMutableURLRequest *sRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:sRequestURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10];
    [sRequest setHTTPMethod:aMethod];
    [sRequest setHTTPBody:aPostData];
    [aHeaders enumerateKeysAndObjectsUsingBlock:^(id aKey, id aObject, BOOL *aStop) {
        [sRequest setValue:aObject forHTTPHeaderField:aKey];
    }];
    
    NSURLConnection *connection = [[[NSURLConnection alloc] initWithRequest:sRequest delegate:self] autorelease];
    if(connection) {
        mReceivedData = [[NSMutableData alloc]init];
        return YES;
    }
    return NO;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)aResponse
{
    NSLog(@"connection");
//    [receivedData setLength:0];
    mResponse = aResponse;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    NSLog(@"didReceiveData");
    [mReceivedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"Error : %@", [error localizedDescription]);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"connectionDidFinishLoading");
    mResultString = [[NSString alloc] initWithData:mReceivedData encoding:NSUTF8StringEncoding];
    NSLog(@"resultString:%@", mResultString);
    if(target)
    {
        [target performSelector:selector withObject:mReceivedData];
    }
}

- (void) setDelegate:(id)aTarget selector:(SEL)aSelector
{
    target = aTarget;
    selector = aSelector;
}

- (BOOL)connection:(NSURLConnection *)connection
    canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
{
//    NSLog(@"canAuthenticateAgainstProtectionSpace");
    return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
//    NSLog(@"didReceiveAuthenticationChallenge");
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust])
        
        if (YES)
        {
            [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
        }
    [challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
    
}

- (void) dealloc
{
    [mReceivedData release];
    [mResponse release];
    [mResultString release];
    [super dealloc];
}
@end
