//
//  LKFriendsViewController.m
//  Links
//
//  Created by rinatear on 13. 2. 11..
//  Copyright (c) 2013년 links. All rights reserved.
//

#import "LKFriendsViewController.h"

@interface LKFriendsViewController ()

@end

@implementation LKFriendsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id) init
{
    self = [super init];
    if (self) {
        UIImage *sImage = [UIImage imageNamed:@"gacha_ball_small_blue.png"];
        UIImage *scaledImage = [UIImage imageWithCGImage:[sImage CGImage] scale:(sImage.scale * 5.0) orientation:(sImage.imageOrientation)];
        self.tabBarItem = [[[UITabBarItem alloc] initWithTitle:@"friends" image:scaledImage tag:0] autorelease];
        self.tabBarItem.badgeValue=@"1";
        UIImage *sBackImage = [UIImage imageNamed:@"bg_interiorshop.jpg"];
        //    UIImage *scaledImag = [UIImage imageWithCGImage:[sBackImage CGImage] scale:(sBackImage.scale) orientation:(sBackImage.imageOrientation)];
        UIImageView *sBaseView = [[UIImageView alloc] initWithImage:sBackImage];
        [[self view] addSubview:sBaseView];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:YES];
}
-(void) viewDidAppear:(BOOL)animated
{
    self.tabBarItem.badgeValue=nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
