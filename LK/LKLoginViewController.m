//
//  LKLoginViewController.m
//  Links
//
//  Created by rinatear on 13. 2. 5..
//  Copyright (c) 2013년 links. All rights reserved.
//

#import "LKLoginViewController.h"
#import "LKHttpRequest.h"
#import "LKFriendsViewController.h"
#import "LKProfileViewController.h"
#import "LKItemViewController.h"
#import "LKGameViewController.h"
#import "LKConfigViewController.h"
#import "LKEncryptor.h"
#import "LKCommonUserInfo.h"
//#import "CommonCrypto/CommonDigest.h"

@interface LKLoginViewController ()
{
    UITextField *emailText;
    UITextField *pwText;
}
@end


@implementation LKLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:self.view.window];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:self.view.window];
    
    float sWidth= [self view].bounds.size.width;
    float sHeight= [self view].bounds.size.height;
    [self view].backgroundColor=[UIColor blackColor];
    emailText = [[[UITextField alloc] initWithFrame:CGRectMake(sWidth*0.1, sHeight*0.6, sWidth*0.8, 30)] autorelease];
    emailText.placeholder=@"email";
    emailText.textColor = [UIColor lightGrayColor];
    emailText.delegate = self;
    [[self view] addSubview:emailText];
    emailText.returnKeyType = UIReturnKeyDone;
    emailText.keyboardType = UIKeyboardTypeEmailAddress;
    emailText.backgroundColor = [UIColor whiteColor];
    [[self view] addSubview:emailText];

    pwText = [[[UITextField alloc] initWithFrame:CGRectMake(sWidth*0.1, sHeight*0.7, sWidth*0.8, 30)] autorelease];
    pwText.textColor = [UIColor lightGrayColor];
    pwText.placeholder=@"password";
    pwText.delegate = self;
    pwText.returnKeyType = UIReturnKeyDone;
    pwText.keyboardType = UIKeyboardTypeAlphabet;
    pwText.secureTextEntry=YES;
    pwText.backgroundColor = [UIColor whiteColor];
    [[self view] addSubview:pwText];

    UIButton *sLoginBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [sLoginBtn setTitle:@"login" forState:UIControlStateNormal];
    [sLoginBtn addTarget:self action:@selector(loginBtn:) forControlEvents:UIControlEventTouchUpInside];
    [[self view] addSubview:sLoginBtn];
    [sLoginBtn setFrame:CGRectMake(sWidth*0.1, sHeight*0.8, sWidth*0.8, 30)];
}

- (void) keyboardWillShow:(NSNotification *) notification
{
    NSTimeInterval sAnimationDuration;
    CGRect sFrameBegin;
    CGRect sFrameEnd;
    NSDictionary *sUserInfo = [notification userInfo];
    [[sUserInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] getValue:&sFrameBegin];
    [[sUserInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&sFrameEnd];
    [[sUserInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&sAnimationDuration];
    
    if(self.view.frame.origin.y >= 0)
    {
        [self setViewMoviedUp:YES height:sFrameBegin.size.height duration:sAnimationDuration];
    }
    else
    {
        [self setViewMoviedUp:NO height:sFrameEnd.size.height duration:sAnimationDuration];
    }
}


- (void) keyboardWillHide:(NSNotification *) notification
{
    NSTimeInterval sAnimationDuration;
    CGRect sFrameBegin;
    CGRect sFrameEnd;
    NSDictionary *sUserInfo = [notification userInfo];
    [[sUserInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] getValue:&sFrameBegin];
    [[sUserInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&sFrameEnd];
    [[sUserInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&sAnimationDuration];
    
    if(self.view.frame.origin.y >= 0)
    {
        [self setViewMoviedUp:YES height:sFrameBegin.size.height duration:sAnimationDuration];
    }
    else
    {
        [self setViewMoviedUp:NO height:sFrameEnd.size.height duration:sAnimationDuration];
    }
}
- (void) setViewMoviedUp:(BOOL)movedUp height:(float)height duration:(NSTimeInterval)duration
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:duration];
    CGRect rect = self.view.frame;
    if(movedUp)
    {
        rect.origin.y -= height;
        rect.size.height += height;
    } else {
        rect.origin.y += height;
        rect.size.height -= height;
    }
    self.view.frame = rect;
    [UIView commitAnimations];
    
}
- (void) loginBtn: (id) sender
{
//    [self view].resignFirstResponder;
    NSLog(@"loginBtn. email:%@, pw:%@", emailText.text, pwText.text);
    LKHttpRequest *sRequest = [[LKHttpRequest alloc] init];
    NSString *pw = [[[[LKEncryptor alloc] init] sha1Encrypt:@"1234"] autorelease];
    NSString *sURL = [NSString stringWithFormat:@"login.php?id=aa100@a.com&pass=%@", pw];
    NSLog(@"%@", sURL);
    [sRequest requestWithURL:sURL method:@"POST" param:nil headers:nil];
    [sRequest setDelegate:self selector:@selector(didReceiveFinished:)];
}


- (void) didReceiveFinished:(NSData *)aResult
{
    NSLog(@"Received data:\n%@", aResult);
    NSError *sError;
    NSDictionary *sJsonResult = [NSJSONSerialization JSONObjectWithData:aResult options:kNilOptions error:&sError];
    NSLog(@"Received Json:\n%@", sJsonResult);

    if([[sJsonResult valueForKey:@"result"] isEqualToString:@"fail"])
    {
        NSLog(@"reason:%@", [sJsonResult valueForKey:@"reason"]);
        NSLog(@"login fail");
    } else {
        LKCommonUserInfo *sConfig = [[[LKCommonUserInfo alloc] init] autorelease];
        [sConfig login:sJsonResult];
        if([sConfig checkLoggedIn]){
            [self successLogin];
        }
//        [self configSave:[sJsonResult valueForKey:kAuthKey]];
    }
    
}
/*
 "auth_key" = 5d264b2e07fcddaa799c9c2753473a7c8561170b;
 "db_index" = 2;
 lastlogin = "2013-03-13 10:35:41";
 result = success;
 "user_id" = "aa100@a.com";
 "user_index" = 101;
 "user_profileid" = 8368;
 */

- (void) configSave:(NSString*)aAuthKey
{
    NSUserDefaults *UD = [NSUserDefaults standardUserDefaults];
    if(UD) {
        [UD setObject:[NSString stringWithFormat:@"%@", aAuthKey] forKey:kAuthKey];
        [UD synchronize];
    }
}
//
//- (NSString*) sha1:(NSString*)input
//{
//    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
//    NSData *data = [NSData dataWithBytes:cstr length:input.length];
//    
//    uint8_t digest[CC_SHA1_DIGEST_LENGTH];
//    
//    CC_SHA1(data.bytes, data.length, digest);
//    
//    NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA1_DIGEST_LENGTH *2];
//    for(int i=0; i<CC_SHA1_DIGEST_LENGTH; i++)
//    {
//        [output appendFormat:@"%02X", digest[i]];
//    }
//    return [output uppercaseString];
//}
- (void) successLogin{
    
    LKFriendsViewController *sFriendsController = [[LKFriendsViewController alloc]init];
    LKProfileViewController *sProfileController = [[LKProfileViewController alloc]init];
    LKItemViewController *sItemController = [[LKItemViewController alloc]init];
    LKGameViewController *sGameController = [[LKGameViewController alloc]init];
    LKConfigViewController *sConfigController = [[LKConfigViewController alloc]init];
    UITabBarController *sTabController = [[UITabBarController alloc] init];
    [[self navigationController] setNavigationBarHidden:YES];
    sTabController.customizableViewControllers = nil;
    sTabController.viewControllers = [NSArray arrayWithObjects: sFriendsController,sProfileController, sItemController, sGameController, sConfigController, nil];
    
    [[self navigationController] pushViewController:sTabController animated:YES];
    
    [sFriendsController release];
    [sProfileController release];
    [sItemController release];
    [sGameController release];
    [sConfigController release];
}
- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if(textField == pwText) {
        NSLog(@"pwText return");
        [self loginBtn:pwText];
    }
    return YES;
}

- (void) viewWillDisappear:(BOOL)animated
{
    [self view].resignFirstResponder;
}

- (void) viewWillAppear:(BOOL)animated
{
    [[self navigationController] setNavigationBarHidden:NO];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc
{
//    [emailText dealloc];
//    [pwText dealloc];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [super dealloc];
}
@end
