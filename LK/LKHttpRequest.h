//
//  LKHttpRequest.h
//  Links
//
//  Created by rinatear on 13. 2. 12..
//  Copyright (c) 2013년 links. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LKHttpRequest : NSObject
{
    NSMutableData *mReceivedData;
    NSURLResponse *mResponse;
    NSString *mResultString;
    id target;
    SEL selector;
}

- (BOOL) requestWithURL:(NSString *)aURL
                 method:(NSString *) aMethod
                  param:(NSData *)aPostData
                headers:(NSDictionary *)aHeaders;

- (void) setDelegate:(id)aTarget selector:(SEL)aSelector;
@end
