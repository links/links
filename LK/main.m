//
//  main.m
//  LK
//
//  Created by rinatear on 13. 2. 5..
//  Copyright (c) 2013년 links. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LKAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LKAppDelegate class]));
    }
}
