//
//  LKEncryptor.h
//  Links
//
//  Created by rinatear on 13. 2. 18..
//  Copyright (c) 2013년 links. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LKEncryptor : NSObject
- (NSString*) sha1Encrypt:(NSString*)input;
@end
