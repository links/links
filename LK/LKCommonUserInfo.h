//
//  LKCommonConfig.h
//  Links
//
//  Created by rinatear on 13. 3. 12..
//  Copyright (c) 2013년 links. All rights reserved.
//

#import <Foundation/Foundation.h>
static NSString *const kAuthKey = @"auth_key";
static NSString *const kDbIndex = @"db_index";
static NSString *const kLastLogin = @"lastlogin";
static NSString *const kUserId = @"user_id";
static NSString *const kUserIndex = @"user_index";
static NSString *const kUserProfileId = @"user_profileid";

@interface LKCommonUserInfo : NSObject
{
    
}

-(void)login:(NSDictionary*)aJson;
-(BOOL)checkLoggedIn;
-(void)logout;
-(NSString*)objectForKey:(NSString*)aKey;
@end
