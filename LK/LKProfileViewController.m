//
//  LKProfileViewController.m
//  Links
//
//  Created by rinatear on 13. 2. 11..
//  Copyright (c) 2013년 links. All rights reserved.
//

#import "LKProfileViewController.h"
//#import "LKProfileImageView.h"
//#import "LKProfileDetailView.h"
#import "LKProfileView.h"
#import "LKProfileTabHeaderView.h"
#import "LKHttpRequest.h"
#import "LKCommonUserInfo.h"

@interface LKProfileViewController ()

@end

@implementation LKProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id) init
{
    self = [super init];
    if (self) {
        UIImage *sImage = [UIImage imageNamed:@"gacha_ball_small_green.png"];
        UIImage *scaledImage = [UIImage imageWithCGImage:[sImage CGImage] scale:(sImage.scale * 5.0) orientation:(sImage.imageOrientation)];

        //        sImage.size = CGSizeMake(10, 10);
        self.tabBarItem = [[[UITabBarItem alloc] initWithTitle:@"profile" image:scaledImage tag:0] autorelease];
        self.tabBarItem.badgeValue=@"1";
        
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    LKProfileTabHeaderView *sHeaderView = [[[LKProfileTabHeaderView alloc] init] autorelease];
    [[self view] addSubview:sHeaderView];
    [sHeaderView setDelegate:self];

    UIButton *sProfileTab = [[UIButton buttonWithType:UIButtonTypeCustom] autorelease];
    UIImage *tabImg = [UIImage imageNamed:@"Icon.png"];
    [sProfileTab setBackgroundImage:tabImg forState:UIControlStateNormal];
    [sProfileTab addTarget:self action:@selector(profileTapped:) forControlEvents:UIControlEventTouchUpInside];
    [[self view] addSubview:sProfileTab];
    [sProfileTab setFrame:CGRectMake(0, 40, 100, 20)];

    UIButton *sGameTab = [[UIButton buttonWithType:UIButtonTypeCustom] autorelease];
    UIImage *sGameTabImg = [UIImage imageNamed:@"Icon.png"];
    [sGameTab setBackgroundImage:sGameTabImg forState:UIControlStateNormal];
    [sGameTab addTarget:self action:@selector(profileTapped:) forControlEvents:UIControlEventTouchUpInside];
    [[self view] addSubview:sGameTab];
    [sGameTab setFrame:CGRectMake(100, 40, 100, 20)];
    
    UIButton *sBoardTab = [[UIButton buttonWithType:UIButtonTypeCustom] autorelease];
    UIImage *sboardTabImg = [UIImage imageNamed:@"Icon.png"];
    [sBoardTab setBackgroundImage:sboardTabImg forState:UIControlStateNormal];
    [sBoardTab addTarget:self action:@selector(profileTapped:) forControlEvents:UIControlEventTouchUpInside];
    [[self view] addSubview:sBoardTab];
    [sBoardTab setFrame:CGRectMake(200, 40, 100, 20)];
    
    //sub tab
//    UIView *sSubTabView = [[[UIView alloc] initWithFrame:CGRectMake(0, 40, sWidth, 40)] autorelease];
//    [sSubTabView setBackgroundColor:[UIColor whiteColor]];
//    UITextField *sSubTitle = [[[UITextField alloc] initWithFrame:CGRectZero] autorelease];
//    [sSubTitle setText:@"subTab"];
//    [[self view] addSubview:sSubTitle];
//    [sSubTitle setFrame:CGRectMake(0, 0, 100, 40)];
    
    
    mProfileView = [[[LKProfileView alloc] initWithFrame:CGRectMake(0, 60, 320, 400)] autorelease];
    [[self view] addSubview:mProfileView];
    
    
//    [sProfileDetailView setFrame:CGRectMake(20, 130, 250, 300)];
//    
//    UIButton *sProfileBtn = [UIButton buttonWithType:UIButtonTypeCustom];
////    [sProfileBtn setTitle:@"profile" forState:UIControlStateNormal];
//    [sProfileBtn setFrame:CGRectMake(0, 0, 100, 30)];
//    UIImage *sProfileBtnImage = [UIImage imageNamed:@"profile_page_savebutton.png"];
//    [sProfileBtn setBackgroundImage:sProfileBtnImage forState:UIControlStateNormal];
//    [sProfileBtn addTarget:self action:@selector(profileTapped:) forControlEvents:UIControlEventTouchUpInside];
//    [sSubTabView addSubview:sProfileBtnImage];
//    [sSubTabView addSubview:sProfileBtn];

//    [sProfileBtn sizeToFit];
//    [[self view] addSubview:sSubTabView];
    
	// Do any additional setup after loading the view.
    
    
    mCheckbox = [[[UIButton alloc] initWithFrame:CGRectZero] autorelease];
    
    [mCheckbox setImage:[UIImage imageNamed:@"checkbox_unchecked.png"] forState:UIControlStateNormal];
    [mCheckbox setImage:[UIImage imageNamed:@"checkbox-checked-hi.png"] forState:UIControlStateSelected];
    [mCheckbox addTarget:self action:@selector(checkboxDidTapped:) forControlEvents:UIControlEventTouchUpInside];
    [mCheckbox setAdjustsImageWhenHighlighted:YES];
    [[self view] addSubview:mCheckbox];
    [mCheckbox setFrame:CGRectMake(40, 370, 20, 20)];
    
}

- (void)profile
{
LKHttpRequest *sRequest = [[LKHttpRequest alloc] init];
    
//NSString *pw = [[[[LKEncryptor alloc] init] sha1Encrypt:@"1234"] autorelease];
    LKCommonUserInfo *sUserInfo = [[[LKCommonUserInfo alloc] init] autorelease];
    NSString *aAuthKey = [sUserInfo objectForKey:kAuthKey];
    NSString *aDbIndex = [sUserInfo objectForKey:kDbIndex];
NSString *sURL = [NSString stringWithFormat:@"getprofile.php?id=aa100@a.com&db_index=%@&ak=%@", aDbIndex, aAuthKey];
//NSLog(@"%@", sURL);
[sRequest requestWithURL:sURL method:@"POST" param:nil headers:nil];
[sRequest setDelegate:self selector:@selector(didReceiveFinished:)];
}


- (void)didReceiveFinished:(NSData *)aResult
{
    NSLog(@"Received data:\n%@", aResult);
    NSError *sError;
    NSDictionary *sJsonResult = [NSJSONSerialization JSONObjectWithData:aResult options:kNilOptions error:&sError];
    NSLog(@"Received Json:\n%@", sJsonResult);
    
    if([[sJsonResult valueForKey:@"result"] isEqualToString:@"fail"])
    {
        NSLog(@"reason:%@", [sJsonResult valueForKey:@"reason"]);
        NSLog(@"login fail");
    } else {
//        LKCommonUserInfo *sConfig = [[[LKCommonUserInfo alloc] init] autorelease];
//        [sConfig login:sJsonResult];
//        if([sConfig checkLoggedIn]){
//            [self successLogin];
//        }
        //        [self configSave:[sJsonResult valueForKey:kAuthKey]];
    }
    
}


-(void)editButtonDidTapped:(id)aSel
{
    NSLog(@"edit");
}
- (void)checkboxDidTapped:(id)aSel
{
    NSLog(@"check");
    mCheckBoxSelected= !mCheckBoxSelected;
    [mCheckbox setSelected:mCheckBoxSelected];
}

-(void) profileTapped:(id)selector
{
    NSLog(@"profile tapped");
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    self.tabBarItem.badgeValue=nil;
    [[self navigationController] setNavigationBarHidden:YES];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
