//
//  LKItemViewController.m
//  Links
//
//  Created by rinatear on 13. 2. 14..
//  Copyright (c) 2013년 links. All rights reserved.
//

#import "LKItemViewController.h"

@interface LKItemViewController ()

@end

@implementation LKItemViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id) init
{
    self = [super init];
    if (self) {
        UIImage *sImage = [UIImage imageNamed:@"gacha_ball_small_red.png"];
         UIImage *scaledImage = [UIImage imageWithCGImage:[sImage CGImage] scale:(sImage.scale * 5.0) orientation:(sImage.imageOrientation)];
        //        sImage.size = CGSizeMake(10, 10);
        self.tabBarItem = [[[UITabBarItem alloc] initWithTitle:@"item" image:scaledImage tag:0] autorelease];
        self.tabBarItem.badgeValue=@"1";
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:YES];
	// Do any additional setup after loading the view.
}

-(void) viewDidAppear:(BOOL)animated
{
    self.tabBarItem.badgeValue=nil;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
