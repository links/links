//
//  LKMainViewController.m
//  Links
//
//  Created by rinatear on 13. 2. 5..
//  Copyright (c) 2013년 links. All rights reserved.
//

#import "LKMainViewController.h"
#import "LKLoginViewController.h"
#import "LKAgreementsViewController.h"
#import "LKFriendsViewController.h"
#import "LKProfileViewController.h"
#import "LKItemViewController.h"
#import "LKGameViewController.h"
#import "LKConfigViewController.h"
#import "LKCommonUserInfo.h"

@interface LKMainViewController ()
{
    NSString *gSession;
}
@end

@implementation LKMainViewController
//
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}
//

- (id) init
{
    self = [super init];
    if(self)
    {
        
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
        
    UIImage *sBackImage = [UIImage imageNamed:@"bg_interiorshop.jpg"];
    //    UIImage *scaledImag = [UIImage imageWithCGImage:[sBackImage CGImage] scale:(sBackImage.scale) orientation:(sBackImage.imageOrientation)];
    UIImageView *sBaseView = [[UIImageView alloc] initWithImage:sBackImage];
    [[self view] addSubview:sBaseView];
    	// Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    float sWidth= [self view].bounds.size.width;
    float sHeight= [self view].bounds.size.height;

//    [self configLoad];
    LKCommonUserInfo *aConfig = [[[LKCommonUserInfo alloc] init] autorelease];
    if(![aConfig checkLoggedIn]) {
        //need to login view
        
        UIButton *sFaceBookLoginBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [sFaceBookLoginBtn setTitle:@"facebook login" forState:UIControlStateNormal];
        [sFaceBookLoginBtn addTarget:self action:@selector(loginToFaceBook:) forControlEvents:UIControlEventTouchUpInside];
        UIButton *sLoginBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [sLoginBtn setTitle:@"login" forState:UIControlStateNormal];
        [sLoginBtn addTarget:self action:@selector(loginToLinks:) forControlEvents:UIControlEventTouchUpInside];
        UIButton *sJoinBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [sJoinBtn setTitle:@"join" forState:UIControlStateNormal];
        [sJoinBtn addTarget:self action:@selector(joinToLinks:) forControlEvents:UIControlEventTouchUpInside];
        
        //add views
        [[self view] addSubview:sFaceBookLoginBtn];
        [[self view] addSubview:sLoginBtn];
        [[self view] addSubview:sJoinBtn];
        
        
        //views position
        [sFaceBookLoginBtn setFrame:CGRectMake(sWidth*0.1, 50, sWidth*0.8, 30)];
        
        [sLoginBtn setFrame:CGRectMake(sWidth*0.1, 100, sWidth*0.8, 30)];
        [sJoinBtn setFrame:CGRectMake(sWidth*0.1, 150, sWidth*0.8, 30)];
        
    } else {
        [self goTab];
    }
    
    /////
    UIButton *sTabBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [sTabBtn setTitle:@"main" forState:UIControlStateNormal];
    [sTabBtn addTarget:self action:@selector(goTab) forControlEvents:UIControlEventTouchUpInside];
    [[self view] addSubview:sTabBtn];
    [sTabBtn setFrame:CGRectMake(sWidth*0.1, sHeight*0.8, sWidth*0.8, 30)];
    

    
    [[self navigationController] setNavigationBarHidden:YES];
}
- (void) loginToFaceBook:(id)sender
{
    NSLog(@"loginToFaceBook");
}


- (void) loginToLinks:(id)sender
{
    NSLog(@"loginToLinks");
    LKLoginViewController *sLinksLoginViewController = [[[LKLoginViewController alloc]init]autorelease];
    [[self navigationController] pushViewController:sLinksLoginViewController animated:YES];
}

- (void) joinToLinks:(id)sender
{
    NSLog(@"joinToLinks");
    LKAgreementsViewController *sAgreementsViewController = [[[LKAgreementsViewController alloc]init]autorelease];
    [[self navigationController] pushViewController:sAgreementsViewController animated:YES];
}

- (void) goTab
{
    NSLog(@"tab");
    UITabBarController *sTabController = [[UITabBarController alloc] init];
    sTabController.customizableViewControllers = nil;
    
    LKFriendsViewController *sFriendsController = [[LKFriendsViewController alloc]init];
    LKProfileViewController *sProfileController = [[LKProfileViewController alloc]init];
    LKItemViewController *sItemController = [[LKItemViewController alloc]init];
    LKGameViewController *sGameController = [[LKGameViewController alloc]init];
    LKConfigViewController *sConfigController = [[LKConfigViewController alloc]init];
    sTabController.viewControllers = [NSArray arrayWithObjects: sFriendsController,sProfileController, sItemController, sGameController, sConfigController, nil];

    [[self navigationController] pushViewController:sTabController animated:YES];
    
    [sFriendsController release];
    [sProfileController release];
    [sItemController release];
    [sGameController release];
    [sConfigController release];
}

- (void) configLoad
{
    //    NSUserDefaults
    NSUserDefaults *UD = [NSUserDefaults standardUserDefaults];
    if(UD) {
        gSession = [UD objectForKey:@"auth_key"];
    }
    
}
//
//- (void) configSave
//{
//    NSUserDefaults *UD = [NSUserDefaults standardUserDefaults];
//    if(UD) {
//        [UD setObject:[NSString stringWithFormat:@"%@", gSession] forKey:@"session"];
//        [UD synchronize];
//    }
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
